;;; package --- Summary
;;; Commentary:
;;; code:  
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(blink-cursor-mode nil)
 '(column-number-mode t)
 '(custom-enabled-themes (quote (deeper-blue)))
 '(custom-safe-themes
   (quote
	("29a4267a4ae1e8b06934fec2ee49472daebd45e1ee6a10d8ff747853f9a3e622" "2fbd9f9d152ead7642de136f545bf4ec232d9b4f80aafad7bbe7512e093b7f3b" "978bd4603630ecb1f01793af60beb52cb44734fc14b95c62e7b1a05f89b6c811" "e26780280b5248eb9b2d02a237d9941956fc94972443b0f7aeec12b5c15db9f3" "dc46381844ec8fcf9607a319aa6b442244d8c7a734a2625dac6a1f63e34bc4a6" "f220c05492910a305f5d26414ad82bf25a321c35aa05b1565be12f253579dec6" "c7359bd375132044fe993562dfa736ae79efc620f68bab36bd686430c980df1c" "d293542c9d4be8a9e9ec8afd6938c7304ac3d0d39110344908706614ed5861c9" "61d1a82d5eaafffbdd3cab1ac843da873304d1f05f66ab5a981f833a3aec3fc0" "d0ff5ea54497471567ed15eb7279c37aef3465713fb97a50d46d95fe11ab4739" "36a309985a0f9ed1a0c3a69625802f87dee940767c9e200b89cdebdb737e5b29" default)))
 '(display-time-mode t)
 '(inhibit-startup-screen t)
 '(linum-format " %7i ")
 '(menu-bar-mode nil)
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;;;;;;;;;;;;;;;;;;; mode ;;;;;;;;;;;;;;;;;;;;
;; cscope
;; (add-to-list 'load-path' "~/.emacs.d/site-lisp")
;; (require 'xcscope)
;; (add-hook 'c-mode-common-hook
;;	  '(lambda ()
;; 	    (require 'xcscope)))

;(add-to-list 'load-path "~/.emacs.d/site-lisp")
;(window-numbering-mode t)

;;;;;;;;;;;;;;;;;;;; display setting ;;;;;;;;;;;;;;;;;;;;
;Color Theme
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(load-theme 'deeper-blue t)

;普通设置
(setq x-select-enable-clipboard t);允许emacs和外部其他程序的粘贴
(setq column-number-mode t);显示列号
(global-linum-mode t);显示行号
(setq default-fill-column 80);显示80列换行

;; 显示时间，格式如下
(setq display-time-24hr-format t)
(setq display-time-interval 10)
(global-font-lock-mode 1);开启语法高亮
(setq default-tab-width 4)
(setq c-basic-offset 4)
(mouse-avoidance-mode 'animate);鼠标自动避开指针
(transient-mark-mode 1);高亮显示要拷贝的内容
(show-paren-mode 1);自动显示所匹配的括号
(mouse-wheel-mode t);是用滚轴鼠标


(set-language-environment 'UTF-8) 
(set-locale-environment "UTF-8") 
(set-default-font "Monaco-12") 
(set-fontset-font "fontset-default" 'unicode '("WenQuanYi Zen Hei" . "unicode-ttf"))

; font
;; Chinese Font
;(dolist (charset '(kana han symbol cjk-misc bopomofo))
;  (set-fontset-font (frame-parameter nil 'font)
;                    charset (font-spec :family "文泉驿等宽正黑"
;                                       :size 13)))

;;;;;;;;;;;;;;;;;;;; haskell mode ;;;;;;;;;;;;;;;;;;;;

(add-to-list 'load-path "~/.emacs.d/site-lisp/haskell-mode")
(load "~/.emacs.d/site-lisp/haskell-mode/haskell")
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
;;(add-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)

;;;;;;;;;;;;;;;;;;;; set-key ;;;;;;;;;;;;;;;;;;;;

;自定义按键
(global-set-key [f1] 'shell);F1进入Shell
(global-set-key [f2] 'call-last-kbd-macro);F2调用最后一个键盘宏
(global-set-key [f8] 'other-window);F8窗口间跳转
(global-set-key [f9]  'split-window-horizontally);F9垂直分割窗口
(global-set-key [f10] 'split-window-vertically);F10水平分割窗口
(global-set-key [f11] 'delete-other-windows);F11 关闭其它窗口
(global-set-key [f12] 'toggle-fullscreen);F12 全屏
(global-set-key [C-return] 'kill-this-buffer);C-return关闭当前buffer
(global-set-key (kbd "C-,") 'backward-page);文件首
(global-set-key (kbd "C-.") 'forward-page);文件尾
(global-set-key "\C-x\C-m" 'execute-extended-command);把M-x绑定到C-x C-m
(global-set-key "\C-xm" nil);关闭E-mail快捷键
(global-set-key "\C-w" 'backward-kill-word)
(global-set-key "\C-x\C-k" 'kill-region)
(fset 'yes-or-no-p 'y-or-n-p);以 y/n代表 yes/no
(setq-default kill-whole-line nil);使用C-k删掉指针到改行末的所有东西

;;;;;;;;;;;;;;;;;;;; misc ;;;;;;;;;;;;;;;;;;;;

;备份设置
;emacs还有一个自动保存功能，默认在~/.emacs.d/auto-save-list里
(setq version-control t);启用版本控制，即可以备份多次
(setq kept-old-versions 2);备份最原始的版本两次，即第一次编辑前的文档，和第二次编辑前的文档
(setq kept-new-versions 5);备份最新的版本五次，理解同上
(setq delete-old-versions t);删掉不属于以上7中版本的版本
(setq backup-directory-alist '(("." . "~/.emacs.tmp")));设置备份文件的路径
(setq backup-by-copying t);备份设置方法，直接拷贝
(setq auto-save-mode t);自动存盘
(setq visible-bell nil);去掉警告铃声
(setq scroll-step 1;滚动页面时比较舒服，不要整页的滚动
        scroll-margin 3
        scroll-conservatively 10000)
(setq kill-ring-max 200);设定删除保存记录为200，可以方便以后无限恢复
(setq-default ispell-program-name "aspell");是用aspell程序作为Emacs的拼写检查程序
(put 'upcase-region 'disabled nil)

;; Set Emacs to save buffers on exit
;(require 'desktop) 
;(desktop-save-mode t)
;(defun my-desktop-save ()
;  (interactive)
;  ;; Don't call desktop-save-in-desktop-dir, as it prints a message.
;  (if (eq (desktop-owner) (emacs-pid))
;	  (desktop-save desktop-dirname)))
;(add-hook 'auto-save-hook 'my-desktop-save)
