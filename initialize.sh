
# zsh
cd
sudo apt-get -y install zsh
ln -s ~/.dotfiles/zsh/zshrc.symlink .zshrc

# screen
cd
sudo apt-get -y install zsh

# git
cd
sudo apt-get -y install git

# emacs
cd
sudo apt-get -y install emacs
ln -s ~/.dotfiles/emacs/emacsd ~/.emacs.d
ln -s ~/.dotfiles/emacs/emacs.emacs ~/.emacs
